scalaVersion := "3.2.2"

resolvers += MavenCache("local-maven", file("/home/mackler/.m2/repository"))

libraryDependencies ++= Seq(
  "com.typesafe" % "config" % "1.4.2",
  "org.spayd" % "ShortPaymentGenerator" % "0.0.1"
)
