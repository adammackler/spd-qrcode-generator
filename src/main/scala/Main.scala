package com.upisom.invoice

import org.spayd.utilities.SpaydQRUtils
import org.spayd.utilities.SpaydConstants.*
import org.spayd.model.account.BankAccount
import com.typesafe.config.{Config, ConfigFactory}
import javax.imageio.ImageIO
import java.io.File
import java.awt.image.BufferedImage
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class SlovakBankAccount(accountNumber: String, bic: String):
  /*
   * https://bank.codes/iban/structure/slovakia/
   * 2 letters ISO country code
   * 2 digits IBAN check digits
   * 4 digits bank code
   * 6 digits account prefix
   * 10 digits account number
   * example: SK 31 1200 000019 8742637541
   */
  require(accountNumber.matches("^[A-^]{2}[0-9]{22}$"))
  override def toString = accountNumber + "+" + bic

val dateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
@main def main() =
  val conf: Config = ConfigFactory.load()
  val bankAccount = new SlovakBankAccount(conf.getString("iban"), conf.getString("bic"))
  val amount = BigDecimal.exact(conf.getString("amount"))
  val today = LocalDate.now
  val dueDate = LocalDate.parse(conf.getString("dueDate"))
  val todayString = today.format(dateFormatter)
  val currency = conf.getString("currency")
  require(currency.matches("^[A-Z]{3}$"))
  val payeeName = conf.getString("payeeName")
  val sendersReference = conf.getInt("sendersReference")

  val paymentDetails = Map[String,String](
    "ACC" -> bankAccount.toString,
    "AM" -> amount.setScale(2).toString,
    "CC" -> currency,
    "RF" -> sendersReference.toString,
    "RN" -> payeeName,
    "DT" -> dueDate.format(dateFormatter),
    "MSG" -> conf.getString("paymentMessage"),
    "X-VS" -> conf.getInt("X-VS").toString,
    "X-SS" -> conf.getInt("X-SS").toString,
    "X-KS" -> conf.getInt("X-KS").toString,
  )

  val paymentString = paymentDetails.map {(k,v) => k + ":" + v }.mkString("SPD*1.0*", "*", "")

  println(s"output $paymentString")

  val imageSize = defQRSize
  val image: BufferedImage = SpaydQRUtils.getQRCode(20, paymentString)
  val filename = s"payment-code-$todayString.png"
  if ImageIO.write(image, "png", new File(filename))
  then println(s"wrote file $filename")
  else println("failed to write image file to $filename")
