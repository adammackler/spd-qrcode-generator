Modify the java ShortPaymentGenerator library:

    <properties>
      ..
      <maven.compiler.target>11</maven.compiler.target>
      <maven.compiler.source>11</maven.compiler.source>
    </properties>

Remove `import com.sun.media.sound.InvalidFormatException;` from `src/org/spayd/utilities/SpaydQRUtils.java`,
and change

    throw new InvalidFormatException();

to

    throw new IOException("missing matrix or barsize");

Build and install using maven:

    mvn install

other useful commands:

    mvn compile
    mvn test
    mvn package
    mvn -help

In the `src/main/resources/` directory, copy `application.conf.example` into `application.conf` and edit with
the payment details specific to the invoice.

# Run

    sbt run

QR code is written to a PNG file with the date in its name.

To check it, use the `zbarimg` command (part of `sbar-tools` package.

# References

- [Short Payment Descriptor on Wikipedia](https://en.wikipedia.org/wiki/Short_Payment_Descriptor)
- [Standard No. 26: Format for exchanging the payment information for Czech domestic payments using the QR codes](https://cbaonline.cz/format-pro-sdileni-platebnich-udaju-v-czk-qr-kody)
